﻿Imports System.IO.Compression
Imports ImageScroller.Common
Imports Windows.Storage
Imports Windows.Storage.Pickers
Imports Windows.Storage.Streams
Imports Windows.Graphics.Imaging
Imports Windows.UI.Popups
Imports Windows.UI.Core

' The Items Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234233

''' <summary>
''' A page that displays a collection of item previews.  In the Split Application this page
''' is used to display and select one of the available groups.
''' </summary>
Public NotInheritable Class TitlePage
    Inherits Page

    Public Const RecentItemLimit As Integer = 10
    Private browseFolderItem As SpecialFolderItem
    Private browseZipFile As SpecialFolderItem
    Private browseZipFileWithOptimization As SpecialFolderItem

    ''' <summary>
    ''' NavigationHelper is used on each page to aid in navigation and
    ''' process lifetime management
    ''' </summary>
    Public ReadOnly Property NavigationHelper As Common.NavigationHelper
        Get
            Return Me._navigationHelper
        End Get
    End Property
    Private _navigationHelper As Common.NavigationHelper

    ''' <summary>
    ''' This can be changed to a strongly typed view model.
    ''' </summary>
    Public ReadOnly Property DefaultViewModel As Common.ObservableDictionary
        Get
            Return Me._defaultViewModel
        End Get
    End Property
    Private _defaultViewModel As New Common.ObservableDictionary()

    Public Sub New()
        InitializeComponent()
        Me._navigationHelper = New Common.NavigationHelper(Me)
        AddHandler Me._navigationHelper.LoadState, AddressOf NavigationHelper_LoadState
        AddHandler Me._navigationHelper.SaveState, AddressOf NavigationHelper_SaveState

        browseFolderItem = New SpecialFolderItem() With {.Title = "Browse folder...", .SpecialSymbol = Symbol.OpenLocal, .ToolTip = "Browse to a folder and open the images in it."}
        browseZipFile = New SpecialFolderItem() With {.Title = "Open zip file...", .SpecialSymbol = Symbol.OpenFile, .ToolTip = "Extract a zip file to local storage and open it."}
        browseZipFileWithOptimization = New SpecialFolderItem() With {.Title = "Open zip file with optimization...", .SpecialSymbol = Symbol.OpenFile, .ToolTip = "Extract a zip file to local storage and open it, optimizing the images to your current view."}
    End Sub

    ''' <summary>
    ''' Populates the page with content passed during navigation.  Any saved state is also
    ''' provided when recreating a page from a prior session.
    ''' </summary>
    ''' <param name="sender">
    ''' The source of the event; typically <see cref="NavigationHelper"/>
    ''' </param>
    ''' <param name="e">Event data that provides both the navigation parameter passed to
    ''' <see cref="Frame.Navigate"/> when this page was initially requested and
    ''' a dictionary of state preserved by this page during an earlier
    ''' session.  The state will be null the first time a page is visited.</param>
    Private Async Sub NavigationHelper_LoadState(sender As Object, e As Common.LoadStateEventArgs)
        FolderItems = Await App.Current.FolderItems
        DefaultViewModel("Items") = FolderItems

        ' Prevent item grid view auto-selecting first item
        itemGridView.SelectedItem = Nothing

        ' Load folder thumbnails
        For Each x In FolderItems
            Dim t = x.LoadThumbnailAsync()
        Next

        ' Remove special items
        For Each x In FolderItems.Where(Function(a) TypeOf a Is SpecialFolderItem).ToList()
            FolderItems.Remove(x)
        Next

        ' Add special items
        FolderItems.Add(browseFolderItem)
        FolderItems.Add(browseZipFile)
        FolderItems.Add(browseZipFileWithOptimization)
    End Sub

    Private Async Sub NavigationHelper_SaveState(sender As Object, e As Common.SaveStateEventArgs)
        Await SaveStateAsync()
    End Sub

    ''' <summary>
    ''' Asynchronously saves the state for the page, such as the recent items list.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Async Function SaveStateAsync() As Task
        Await App.Current.SaveFolderItemsAsync()
    End Function

#Region "NavigationHelper registration"

    ''' The methods provided in this section are simply used to allow
    ''' NavigationHelper to respond to the page's navigation methods.
    '''
    ''' Page specific logic should be placed in event handlers for the
    ''' <see cref="Common.NavigationHelper.LoadState"/>
    ''' and <see cref="Common.NavigationHelper.SaveState"/>.
    ''' The navigation parameter is available in the LoadState method
    ''' in addition to page state preserved during an earlier session.

    Protected Overrides Sub OnNavigatedTo(e As NavigationEventArgs)
        _navigationHelper.OnNavigatedTo(e)
    End Sub

    Protected Overrides Sub OnNavigatedFrom(e As NavigationEventArgs)
        _navigationHelper.OnNavigatedFrom(e)
    End Sub

#End Region

    Private Async Sub Button_Click(sender As Object, e As RoutedEventArgs)
        Await BrowseForFolderAsync()
    End Sub

    ''' <summary>
    ''' Presents a folder picker to the user and opens its contents.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Async Function BrowseForFolderAsync() As Task
        ' Show picker
        Dim picker As New FolderPicker() With {.SuggestedStartLocation = PickerLocationId.PicturesLibrary}
        picker.FileTypeFilter.Add("*")
        Dim folder As StorageFolder = Await picker.PickSingleFolderAsync()
        Await BrowseForFolderAsync(folder)
    End Function

    ''' <summary>
    ''' Opens the specified folder.
    ''' </summary>
    ''' <param name="folder">The folder to open.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Async Function BrowseForFolderAsync(folder As StorageFolder) As Task
        If folder Is Nothing Then Return

        ' Save folder to recent items
        If Not (From x In FolderItems Where x.FolderPath = folder.Path Select x).Any() Then
            Dim newItem As New FolderItem() With {.Title = folder.DisplayName, .Folder = folder}
            FolderItems.Insert(0, newItem)
            If FolderItems.Count - 1 > RecentItemLimit Then
                FolderItems.RemoveAt(RecentItemLimit)
            End If
        End If

        ' Save state
        Await SaveStateAsync()

        ' Navigate to scroller
        Frame.Navigate(GetType(ImageScrollerPage), folder.Path)
    End Function

    Friend Property FolderItems As ObservableCollection(Of FolderItem)

    Private Sub itemGridView_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles itemGridView.SelectionChanged
        Dim isSpecial As Boolean = TypeOf itemGridView.SelectedItem Is SpecialFolderItem
        Dim open As Boolean = itemGridView.SelectedItem IsNot Nothing AndAlso Not isSpecial
        bottomAppBar.IsOpen = open

        ' Deselect any selected special items
        If isSpecial Then
            itemGridView.SelectedItem = Nothing
        End If
    End Sub

    Private Async Sub itemGridView_ItemClick(sender As Object, e As ItemClickEventArgs) Handles itemGridView.ItemClick
        If e.ClickedItem Is browseFolderItem Then
            Await BrowseForFolderAsync()
            Return
        ElseIf e.ClickedItem Is browseZipFile Then
            Await BrowseForZipFileAsync()
            Return
        ElseIf e.ClickedItem Is browseZipFileWithOptimization Then
            ' Check what width the user wants to use.
            If Not ApplicationView.GetForCurrentView().IsFullScreen Then
                Dim message As New MessageDialog("The application is not running in full screen. Do you want to use the current window width as the maximum image width?")
                Dim confirm As New UICommand("Use current window width")
                Dim cancel As New UICommand("Cancel")
                message.Commands.Add(confirm)
                message.Commands.Add(cancel)
                message.CancelCommandIndex = 1
                Dim result As IUICommand = Await message.ShowAsync()
                If result Is cancel Then Return
            End If

            Await BrowseForZipFileAsync(True)
            Return
        End If

        ' Open the selected folder item.
        Dim item As FolderItem = DirectCast(e.ClickedItem, FolderItem)
        Await BrowseForFolderAsync(item.Folder)
    End Sub

    ''' <summary>
    ''' Presents a file picker to the user for opening a zip file,
    ''' extracts it to local storage and opens it,
    ''' optionally resizing the images in it.
    ''' </summary>
    ''' <param name="resizeToScreen">Whether to use the current window width as maximum width.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Async Function BrowseForZipFileAsync(resizeToScreen As Boolean) As Task
        ' Show a picker
        Dim picker As New FileOpenPicker() With {.SuggestedStartLocation = PickerLocationId.PicturesLibrary}
        picker.FileTypeFilter.Add(".zip")
        Dim file As StorageFile = Await picker.PickSingleFileAsync()
        If file Is Nothing Then Return
        Dim destinationFolder As StorageFolder = Nothing
        Dim folderName As String = file.DisplayName
        Dim collisionOption As CreationCollisionOption = CreationCollisionOption.FailIfExists

        AreElementsEnabled = False
        Using deferred As New Deferred(Sub() AreElementsEnabled = True)
            ' Check if the folder exists
            Dim localFolder As StorageFolder = ApplicationData.Current.LocalFolder
            Dim existingFolder As StorageFolder = Await localFolder.GetFolderOrNullAsync(folderName)
            If existingFolder IsNot Nothing Then
                Dim result As String = Await PopUpFolderExistsPopup()
                If result = "Cancel" Then Return
                If result = "OpenExisting" Then
                    Await BrowseForFolderAsync(existingFolder)
                    Return
                End If
                If result = "CreateUnique" Then
                    collisionOption = CreationCollisionOption.GenerateUniqueName
                ElseIf result = "ReplaceExisting"
                    collisionOption = CreationCollisionOption.ReplaceExisting
                ElseIf result = "ExtractTo"
                    collisionOption = CreationCollisionOption.OpenIfExists
                End If
            End If
        End Using

        IsEnabled = False
        progressBar1.Visibility = Visibility.Visible
        progressBar1.IsIndeterminate = True
        Using deferred As New Deferred(
            Sub()
                IsEnabled = True
                progressBar1.Visibility = Visibility.Collapsed
            End Sub)

            destinationFolder = Await ApplicationData.Current.LocalFolder.CreateFolderAsync(folderName, collisionOption)

            ' Create folder to extract to
            Await UnzipToFolderAsync(file, destinationFolder, resizeToScreen, New Progress(Of Double)(AddressOf UnzipProgress))

            ' Create a marker to indicate this is a folder we made.
            Await destinationFolder.CreateFileAsync("marker.dat", CreationCollisionOption.OpenIfExists)

        End Using

        Await BrowseForFolderAsync(destinationFolder)
    End Function

    ''' <summary>
    ''' Presents a file picker to the user for opening a zip file,
    ''' extracts it to local storage and opens it.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Async Function BrowseForZipFileAsync() As Task
        Await BrowseForZipFileAsync(False)
    End Function

    Private Sub UnzipProgress(val As Double)
        progressBar1.IsIndeterminate = False
        progressBar1.Value = val * 100
    End Sub

    ''' <summary>
    ''' Extracts a zip file into the specified folder.
    ''' </summary>
    ''' <param name="zipFile">The zip file to extract.</param>
    ''' <param name="folder">The folder to extract to.</param>
    ''' <param name="resizeToScreen">Whether to resize files to the window width.</param>
    ''' <param name="progress">Progress indicator that will report progress from 0 to 1.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Async Function UnzipToFolderAsync(zipFile As StorageFile, folder As StorageFolder, resizeToScreen As Boolean, progress As IProgress(Of Double)) As Task
        Dim zipStream = Await zipFile.OpenStreamForReadAsync()
        Using archive As New ZipArchive(zipStream, ZipArchiveMode.Read)
            Dim entryIndex As Integer = 0
            Dim entriesCount As Integer = archive.Entries.Count
            For Each entry In archive.Entries
                Await UnzipEntryAsync(entry, folder)
                progress.Report((entryIndex + 1) / entriesCount)
                entryIndex += 1
            Next
        End Using
    End Function

    Private Async Function GetOrCreateSubfolderAsync(folder As StorageFolder, subfolderName As String) As Task(Of StorageFolder)
        Return Await folder.CreateFolderAsync(subfolderName, CreationCollisionOption.OpenIfExists)
    End Function

    ''' <summary>
    ''' Extracts the zip file entry to the specified folder.
    ''' </summary>
    ''' <param name="entry">The zip file entry.</param>
    ''' <param name="destination">The destination folder.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Async Function UnzipEntryAsync(entry As ZipArchiveEntry, destination As StorageFolder) As Task(Of StorageFile)
        Dim entryPath As String = entry.FullName
        Dim currentFolder As StorageFolder = destination

        ' Create each directory in the path.
        Dim subfolders As String() = entryPath.Split("/")
        For Each subfolder In subfolders.Take(subfolders.Length - 1)
            If String.IsNullOrEmpty(subfolder) Then Continue For
            currentFolder = Await GetOrCreateSubfolderAsync(currentFolder, subfolder)
        Next

        ' Check if the entry name is entry (i.e. the entry is a directory)
        If String.IsNullOrEmpty(entry.Name) Then
            Return Nothing
        End If

        ' Extract the entry.
        Using zipStream As Stream = entry.Open()
            Dim uncompressedFile As StorageFile = Await currentFolder.CreateFileAsync(entry.Name, CreationCollisionOption.ReplaceExisting)
            Using uncompressedFileStream As IRandomAccessStream = Await uncompressedFile.OpenAsync(FileAccessMode.ReadWrite)
                Using out As Stream = uncompressedFileStream.AsStreamForWrite()
                    Await zipStream.CopyToAsync(out)
                    Await out.FlushAsync()
                    Return uncompressedFile
                End Using
            End Using
        End Using
    End Function

    ''' <summary>
    ''' Resizes an image file so that its width matches the current window width,
    ''' keeping its aspect ratio.
    ''' </summary>
    ''' <param name="file">The file to resize.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Async Function ResizeFileToScreen(file As StorageFile) As Task
        Dim width As Double = Window.Current.Bounds.Width
        Dim newFile As StorageFile = Await ApplicationData.Current.TemporaryFolder.CreateFileAsync("resized" & file.FileType, CreationCollisionOption.GenerateUniqueName)
        Using sourceStream As IRandomAccessStreamWithContentType = Await file.OpenReadAsync()
            Dim decoder As BitmapDecoder = BitmapDecoder.CreateAsync(sourceStream)
            Using destinationStream As IRandomAccessStream = Await newFile.OpenAsync(FileAccessMode.ReadWrite)
                Dim encoder As BitmapEncoder = BitmapEncoder.CreateForTranscodingAsync(destinationStream, decoder)
                If decoder.PixelWidth > width Then
                    Return
                End If
                Dim originalWidth As UInteger = decoder.PixelWidth
                Dim originalHeight As UInteger = decoder.PixelHeight

                encoder.BitmapTransform.ScaledWidth = width
                encoder.BitmapTransform.ScaledWidth = CUInt(Math.Floor(originalWidth / width * originalHeight))

                Await encoder.FlushAsync()
            End Using
        End Using

        Await newFile.CopyAndReplaceAsync(file)
    End Function

    Private Sub bottomAppBar_Closed(sender As Object, e As Object)
        itemGridView.SelectedItem = Nothing
    End Sub

    Private Async Sub removeButton_Click(sender As Object, e As RoutedEventArgs)
        Dim item As FolderItem = DirectCast(itemGridView.SelectedItem, FolderItem)
        If item Is Nothing Then Return

        ' Check if the folder is one we created.
        Dim folder As StorageFolder = item.Folder
        Dim marker = Await folder.GetFileOrNullAsync("marker.dat")
        If marker IsNot Nothing Then
            ' Delete the folder to free up space.
            Await folder.DeleteAsync(StorageDeleteOption.Default)
        End If

        FolderItems.Remove(item)
        Await SuspensionManager.SaveAsync()
    End Sub

    Private Async Function PopUpFolderExistsPopup() As Task(Of String)
        If folderExistsResult IsNot Nothing Then
            folderExistsResult.SetResult("Cancel")
        End If
        folderExistsResult = New TaskCompletionSource(Of String)()
        popupBackground.Visibility = Visibility.Visible
        folderExistsPopup.IsOpen = True
        Return Await folderExistsResult.Task
    End Function

    Private folderExistsResult As TaskCompletionSource(Of String)

    Private Sub folderExistsPopupButton_Click(sender As Object, e As RoutedEventArgs)
        folderExistsResult.SetResult(CStr(CType(sender, Button).Tag))
        folderExistsPopup.IsOpen = False
    End Sub

    Private Sub folderExistsPopup_Closed(sender As Object, e As Object) Handles folderExistsPopup.Closed
        popupBackground.Visibility = Visibility.Collapsed
    End Sub

    Private Async Sub folderExistsPopup_Opened(sender As Object, e As Object) Handles folderExistsPopup.Opened
        folderExistsGrid.Width = Window.Current.Bounds.Width
        Await Dispatcher.RunAsync(CoreDispatcherPriority.High, AddressOf Noop)
        folderExistsGrid.Measure(New Size(Double.PositiveInfinity, Double.PositiveInfinity))
        Debug.WriteLine(popupBackground.ActualHeight)
        folderExistsPopup.VerticalOffset = popupBackground.ActualHeight / 2 - folderExistsGrid.ActualHeight / 2
        Debug.WriteLine(folderExistsPopup.VerticalOffset)
    End Sub

    Private Sub Noop()
    End Sub

    Public Shared ReadOnly Property AreElementsEnabledProperty As DependencyProperty = DependencyProperty.Register("AreElementsEnabled", GetType(Boolean), GetType(TitlePage), New PropertyMetadata(True))

    Public Property AreElementsEnabled As Boolean
        Get
            Return CBool(GetValue(AreElementsEnabledProperty))
        End Get
        Set(value As Boolean)
            SetValue(AreElementsEnabledProperty, value)
        End Set
    End Property

End Class
