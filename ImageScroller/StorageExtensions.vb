﻿Imports Windows.Storage

Public Module StorageExtensions

    <Extension>
    Public Async Function GetFolderOrNullAsync(folder As StorageFolder, name As String) As Task(Of StorageFolder)
        Try
            Return Await folder.GetFolderAsync(name)
        Catch ex As FileNotFoundException
            Return Nothing
        End Try
    End Function

    <Extension>
    Public Async Function GetFileOrNullAsync(folder As StorageFolder, name As String) As Task(Of StorageFile)
        Try
            Return Await folder.GetFileAsync(name)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

End Module
