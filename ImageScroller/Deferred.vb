﻿
Public Class Deferred
    Implements IDisposable

    Public Sub New()
    End Sub

    Public Sub New(disposed As Action)
        AddHandler Me.Disposed, Sub(sender, e) disposed()
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        OnDisposed(New EventArgs())
    End Sub

    Public Event Disposed As EventHandler

    Protected Overridable Sub OnDisposed(e As EventArgs)
        RaiseEvent disposed(Me, e)
    End Sub

End Class
