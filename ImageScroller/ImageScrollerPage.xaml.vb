﻿Imports Windows.Storage
Imports Windows.Storage.Search
Imports Windows.UI.Popups
Imports System.Reactive.Subjects
Imports System.Reactive.Linq
Imports System.Threading
Imports System.Reactive
Imports ImageScroller.Common

' The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

''' <summary>
''' An empty page that can be used on its own or navigated to within a Frame.
''' </summary>
Public NotInheritable Class ImageScrollerPage
    Inherits Page

    ''' <summary>
    ''' NavigationHelper is used on each page to aid in navigation and
    ''' process lifetime management
    ''' </summary>
    Public ReadOnly Property NavigationHelper As Common.NavigationHelper
        Get
            Return Me._navigationHelper
        End Get
    End Property
    Private _navigationHelper As Common.NavigationHelper

    Public Sub New()
        Images = New ObservableCollection(Of LazyLoadImage)()
        scrollSubject = New Subject(Of Unit)()
        ' Every few moments, save our scroll state.
        scrollSubject _
            .ObserveOn(SynchronizationContext.Current) _
            .Where(Function(x) Not isScrollAsyncRunning) _
            .Sample(New TimeSpan(0, 0, 10)) _
            .ObserveOnDispatcher() _
            .Subscribe(AddressOf SaveStateAsync)
        Me._navigationHelper = New Common.NavigationHelper(Me)
        AddHandler Me._navigationHelper.LoadState, AddressOf NavigationHelper_LoadState
        AddHandler Me._navigationHelper.SaveState, AddressOf NavigationHelper_SaveState

        InitializeComponent()

        AddHandler Window.Current.SizeChanged, AddressOf Window_SizeChanged
    End Sub

    Private Async Sub NavigationHelper_LoadState(sender As Object, e As LoadStateEventArgs)
        Dim cancelled As Boolean = False
        Try
            StripWidth = Window.Current.Bounds.Width

            Dim folderItem As FolderItem = (Await App.Current.FolderItems).Where(Function(x) x.FolderPath = e.NavigationParameter.ToString()).FirstOrDefault()
            If folderItem Is Nothing Then
                folderItem = New FolderItem()
            End If
            If folderItem.Folder Is Nothing Then
                Await PromptInvalidFolderAsync(folderItem)
            End If

            ' Load folder.
            Dim folder As StorageFolder = Await StorageFolder.GetFolderFromPathAsync(e.NavigationParameter.ToString())
            If folder Is Nothing Then
                Await PromptInvalidFolderAsync(folderItem)
            End If
            Await LoadFolderAsync(folder)

            Me.FolderItem = folderItem

            'Await LoadScrollViewerStateAsync()
            Await LoadIndexAsync()
        Catch ex As TaskCanceledException
            cancelled = True
        End Try
    End Sub

    Private Async Function PromptInvalidFolderAsync(item As FolderItem) As Task
        Dim message As New MessageDialog("Invalid folder.")
        message.Commands.Add(New UICommand("OK"))
        Await message.ShowAsync()

        ' Remove the folder from the recent items list.
        Dim tmp = (Await App.Current.FolderItems).Remove(item)

        Throw New TaskCanceledException()
    End Function

    Private Sub NavigationHelper_SaveState(sender As Object, e As SaveStateEventArgs)
        ' SaveScrollViewerState()
        SaveIndex()
    End Sub

    Private Async Function LoadScrollViewerStateAsync() As Task

        IsEnabled = False
        indexLoaderGrid.Visibility = Windows.UI.Xaml.Visibility.Visible

        Dim state = FolderItem.ScrollState
        If state Is Nothing Then
            state = New ScrollState()
            FolderItem.ScrollState = state
        End If
        scrollViewer.ChangeView(state.HorizontalOffset,
                                Nothing,
                                state.ZoomFactor,
                                True)
        Await SlowLoadScrollViewerVerticalOffsetAsync(state.VerticalOffset)

        IsEnabled = True
        indexLoaderGrid.Visibility = Windows.UI.Xaml.Visibility.Collapsed
    End Function

    Private Async Function SlowLoadScrollViewerVerticalOffsetAsync(verticalOffset As Double) As Task
        Dim increments = {768, 512, 128, 32}
        Dim currentOffset As Integer = 0
        For Each increment In increments
            Do While currentOffset + increment < verticalOffset
                Await SetScrollViewerVerticalOffset(currentOffset)
                currentOffset += increment
            Loop
            Do While currentOffset - increment > verticalOffset
                Await SetScrollViewerVerticalOffset(currentOffset)
                currentOffset -= increment
            Loop
        Next
        Await SetScrollViewerVerticalOffset(verticalOffset)
    End Function

    Private Sub SaveScrollViewerState()
        FolderItem.ScrollState.VerticalOffset = scrollViewer.VerticalOffset
        FolderItem.ScrollState.HorizontalOffset = scrollViewer.HorizontalOffset
        FolderItem.ScrollState.ZoomFactor = scrollViewer.ZoomFactor
    End Sub

    Private Async Function LoadFolderAsync(folder As StorageFolder) As Task
        Dim files = Await folder.GetFilesAsync(CommonFileQuery.OrderByName)
        Dim filesOrdered = From x In files Order By x.Name Select x
        Images.Clear()
        For Each file In filesOrdered
            If file.ContentType.StartsWith("image") Then
                Images.Add(New LazyLoadImage(file))
            End If
        Next
    End Function

#Region "NavigationHelper registration"

    ''' The methods provided in this section are simply used to allow
    ''' NavigationHelper to respond to the page's navigation methods.
    '''
    ''' Page specific logic should be placed in event handlers for the
    ''' <see cref="Common.NavigationHelper.LoadState"/>
    ''' and <see cref="Common.NavigationHelper.SaveState"/>.
    ''' The navigation parameter is available in the LoadState method
    ''' in addition to page state preserved during an earlier session.

    Protected Overrides Sub OnNavigatedTo(e As NavigationEventArgs)
        _navigationHelper.OnNavigatedTo(e)
    End Sub

    Protected Overrides Sub OnNavigatedFrom(e As NavigationEventArgs)
        _navigationHelper.OnNavigatedFrom(e)
    End Sub

#End Region

#Region "Properties"

#Region "Images"

    Private Shared ReadOnly _ImagesProperty As DependencyProperty = DependencyProperty.Register("Images", GetType(ObservableCollection(Of LazyLoadImage)), GetType(ImageScrollerPage), New PropertyMetadata(Nothing))

    Public Shared ReadOnly Property ImagesProperty As DependencyProperty
        Get
            Return _ImagesProperty
        End Get
    End Property

    Public Property Images As ObservableCollection(Of LazyLoadImage)
        Get
            Return CType(GetValue(ImagesProperty), ObservableCollection(Of LazyLoadImage))
        End Get
        Private Set(value As ObservableCollection(Of LazyLoadImage))
            SetValue(ImagesProperty, value)
        End Set
    End Property

#End Region

#Region "StripWidth"

    Private Shared ReadOnly _StripWidthProperty As DependencyProperty = DependencyProperty.Register("StripWidth", GetType(Double), GetType(ImageScrollerPage), New PropertyMetadata(0))

    Public Shared ReadOnly Property StripWidthProperty As DependencyProperty
        Get
            Return _StripWidthProperty
        End Get
    End Property

    Public Property StripWidth As Double
        Get
            Return CDbl(GetValue(StripWidthProperty))
        End Get
        Set(value As Double)
            SetValue(StripWidthProperty, value)
        End Set
    End Property

#End Region

    Public Property FolderItem As FolderItem

#End Region

    Private Sub backButton_Click(sender As Object, e As RoutedEventArgs)
        Frame.GoBack()
    End Sub

    Private Sub ItemsControl_ManipulationDelta(sender As Object, e As ManipulationDeltaRoutedEventArgs)
        scrollViewer.ChangeView(scrollViewer.HorizontalOffset - e.Delta.Translation.X, scrollViewer.VerticalOffset - e.Delta.Translation.Y, Nothing, True)
    End Sub

    Private Sub resetZoomButton_Click(sender As Object, e As RoutedEventArgs)
        scrollViewer.ChangeView(Nothing, Nothing, 1)
    End Sub

    Private Sub Window_SizeChanged()
        ' Save the offset.
        Dim itemsContainer As ItemsStackPanel = DirectCast(itemsControl.ItemsPanelRoot, ItemsStackPanel)
        Dim visibleIndex As Integer = itemsContainer.FirstVisibleIndex

        ' Resizing the window will most likely resize the Frame too.
        StripWidth = Window.Current.Bounds.Width

        ' Load the offset.
        'Dim container As FrameworkElement = itemsControl.ContainerFromIndex(visibleIndex)
        'Dim newOffset = container.TransformToVisual(itemsControl).TransformBounds(New Rect(0, 0, container.ActualWidth, container.ActualHeight)).Top
        '
        'scrollViewer.ChangeView(Nothing, newOffset, Nothing)
    End Sub

    Private scrollSubject As Subject(Of Unit)
    Private isScrollAsyncRunning As Boolean

    Private Sub scrollViewer_ViewChanged(sender As Object, e As ScrollViewerViewChangedEventArgs)
        If Not e.IsIntermediate Then
            ' Queue to save our state.
            scrollSubject.OnNext(Unit.Default)
        End If
    End Sub

    Private Async Sub SaveStateAsync(__ As Unit)
        isScrollAsyncRunning = True
        'SaveScrollViewerState()
        SaveIndex()
        isScrollAsyncRunning = False
        Await SuspensionManager.SaveAsync()
    End Sub

    Private Async Function LoadIndexAsync() As Task
        Dim panel As ItemsStackPanel = DirectCast(itemsControl.ItemsPanelRoot, ItemsStackPanel)
        Do While panel.Children.Count = 0
            Await Task.Delay(1)
        Loop

        IsEnabled = False
        indexLoaderGrid.Visibility = Windows.UI.Xaml.Visibility.Visible

        Dim pageIndex = FolderItem.PageIndex
        Dim increments = GetIncrements()
        Dim moved As Boolean = False
        For Each increment In increments
            Debug.WriteLine("Increment set at {0}. {1} < {2}", increment, panel.FirstVisibleIndex, pageIndex)
            moved = False
            Do While panel.FirstVisibleIndex < pageIndex
                moved = True
                Debug.WriteLine("Incrementing offset. {0} < {1}", panel.FirstVisibleIndex, pageIndex)
                Await IncrementScrollViewerVerticalOffset(increment)
            Loop
            Do While panel.FirstVisibleIndex > pageIndex
                moved = True
                Debug.WriteLine("Decrementing offset. {0} > {1}", panel.FirstVisibleIndex, pageIndex)
                Await IncrementScrollViewerVerticalOffset(-increment)
            Loop
            If panel.FirstVisibleIndex = pageIndex Then
                moved = True
                Await IncrementScrollViewerVerticalOffset(-increment)
            End If
        Next

        IsEnabled = True
        indexLoaderGrid.Visibility = Windows.UI.Xaml.Visibility.Collapsed
    End Function

    Private Function GetIncrements() As IEnumerable(Of Integer)
        Return GetIncrementsReverse().Reverse()
    End Function

    Private Iterator Function GetIncrementsReverse() As IEnumerable(Of Integer)
        Dim power As Integer = 5
        Dim items As Integer = 5
        Dim current As Integer = 1
        For i = 1 To items
            Yield current
            current *= power
        Next
    End Function

    Private Async Function IncrementScrollViewerVerticalOffset(verticalOffsetDelta As Double) As Task
        Await SetScrollViewerVerticalOffset(scrollViewer.VerticalOffset + verticalOffsetDelta)
    End Function

    Private Async Function SetScrollViewerVerticalOffset(verticalOffset As Double) As Task(Of Boolean)
        Dim oldVerticalOffset As Double = scrollViewer.VerticalOffset
        If oldVerticalOffset = verticalOffset Then
            Return False
        End If
        If verticalOffset < 0 Then
            Return False
        End If
        Dim scrollableHeight = scrollViewer.ScrollableHeight
        Do While scrollableHeight = 0
            Await Task.Delay(1)
        Loop
        Dim i As Integer = 0
        Dim vo = scrollViewer.VerticalOffset
        Do While vo <> verticalOffset
            Await Task.Delay(1)
            If Not scrollViewer.ChangeView(Nothing, verticalOffset, Nothing, True) Then
                i += 1
                If i Mod 5 = 0 Then
                    Return False
                End If
            End If
            i = 0
            vo = scrollViewer.VerticalOffset
        Loop
        Return True
    End Function

    Private Sub SaveIndex()
        Dim panel As ItemsStackPanel = DirectCast(itemsControl.ItemsPanelRoot, ItemsStackPanel)
        Dim pageIndex = panel.FirstVisibleIndex
        If pageIndex <= 0 Then pageIndex = 0
        If FolderItem IsNot Nothing Then
            FolderItem.PageIndex = pageIndex
        End If
    End Sub

End Class
