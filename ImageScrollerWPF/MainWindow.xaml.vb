﻿Imports System.Windows.Forms
Imports System.IO
Imports System.Collections.ObjectModel

Class MainWindow

    Public Sub New()
        Images = New ObservableCollection(Of LazyLoadImage)()
        InitializeComponent()
    End Sub

    Private Sub MainWindow_Loaded(sender As Object, e As RoutedEventArgs) Handles Me.Loaded
        Dim picker As New FolderBrowserDialog() With {.RootFolder = Environment.SpecialFolder.MyPictures}
        Dim result = picker.ShowDialog()
        If result <> System.Windows.Forms.DialogResult.Cancel Then
            Dim dir As New DirectoryInfo(picker.SelectedPath)
            Images.Clear()
            For Each x In dir.EnumerateFiles()
                Images.Add(New LazyLoadImage(x))
            Next
        End If
    End Sub

#Region "Properties"

#Region "Images"

    Private Shared ReadOnly _ImagesProperty As DependencyProperty = DependencyProperty.Register("Images", GetType(ObservableCollection(Of LazyLoadImage)), GetType(MainWindow), New PropertyMetadata(Nothing))

    Public Shared ReadOnly Property ImagesProperty As DependencyProperty
        Get
            Return _ImagesProperty
        End Get
    End Property

    Public Property Images As ObservableCollection(Of LazyLoadImage)
        Get
            Return CType(GetValue(ImagesProperty), ObservableCollection(Of LazyLoadImage))
        End Get
        Private Set(value As ObservableCollection(Of LazyLoadImage))
            SetValue(ImagesProperty, value)
        End Set
    End Property

#End Region

#End Region

End Class
